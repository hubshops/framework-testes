package br.com.hubshop.exception;

@SuppressWarnings("serial")
public class TestFailedException extends RuntimeException {
	
	public TestFailedException (String mensagem){
		super(mensagem);
	}
}

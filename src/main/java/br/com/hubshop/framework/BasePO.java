package br.com.hubshop.framework;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import br.com.hubshop.framework.datatable.WebTable;

public class BasePO {
	
	private static WebDriver driver  = null;
	
//	protected static String systemUrl = "http://ec2-52-27-204-65.us-west-2.compute.amazonaws.com:8080/";
	
	protected static String systemUrl = "http://localhost:8080/hubshop/";
	
	private static String feedbackMessageCssSelector = "span.ui-growl-title";

	protected static void goToUrl(String url){
		getDriverInstance().navigate().to(url);
	}
	
	public static String getFeedbackMsg(){
		waitForElementApearByLocator(withCssSelector(feedbackMessageCssSelector));
		return  findElement(withCssSelector(feedbackMessageCssSelector)).getText();
	}
	
	protected static boolean feedbackMsgIsSuccessful(){
		return getFeedbackMsg().equals("Operação realizada com sucesso");
	}
	
	public static List<WebElement> getAllFeedbackMsgs(){
		String xpathAllMessages = "//div[@class='ui-growl-item-container ui-state-highlight ui-corner-all ui-helper-hidden ui-shadow']";
		waitForElementApearByLocator(withCssSelector(feedbackMessageCssSelector));
		return  findMultipleElements(withXpath(xpathAllMessages));
	}
	
	protected static WebElement findElement(By by){
		return getDriverInstance().findElement(by);
	}
	
	protected static WebTable findDataTableInsideOf(WebElement element){
		return new WebTable(element.findElement(withTag("table")));
	}
	
	protected static WebElement inside(By by){
		return getDriverInstance().findElement(by);
	}
	
	protected static List<WebElement> findMultipleElements(By by){
		return getDriverInstance().findElements(by);
	}
	
	protected static By withId(String id){
		return By.id(id);
	}
	
	protected static By withTag(String tag){
		return By.tagName(tag);
	}
	
	protected static By withLinkText(String linkText){
		return By.linkText(linkText);
	}
	
	protected static By withCssSelector(String css){
		return By.cssSelector(css);
	}
	
	protected static By withClassName(String className){
		return By.className(className);
	}
	
	protected static By withXpath(String xpath){
		return By.xpath(xpath);
	}

	
	protected static void typeText(String text,WebElement element){
		element.clear();
		element.sendKeys(text);
	}
	
	public static void unmarkCheckbox(WebElement element){
		if(element.getAttribute("class").contains("ui-icon-check")){
			clickAt(element);
		}		
	}
	
	public static void markCheckbox(WebElement element){
		if(element.getAttribute("class").contains("ui-icon-blank")){
			clickAt(element);
		}		
	}
	
	public static void openPanel(WebElement element){
		if(!isPanelOpen(element)){
			clickAt(element);
		}
	}
	
	protected static void clickAt(WebElement element){
		element.click();// do not work on Chrome
//		element.sendKeys(Keys.ENTER);;
	}
	
	protected static void sleepDuring(int seconds){
		try {
			Thread.sleep(seconds*1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
	protected static void clickAndWaitLoadingMessageAt(WebElement element){
		element.click();// do not work on Chrome
//		element.sendKeys(Keys.ENTER);;
		//Wait for loading message
		waitForLoadingMessageDisapear();
	}

	protected static void waitForLoadingMessageDisapear() {
		waitForElementDisapear(withId("dlgStatusDialog_title"));
	}

	protected static void waitForElementDisapear(By locator) {
		WebDriverWait wait = new WebDriverWait(driver, 60000);
		wait.until(ExpectedConditions.invisibilityOfElementLocated(locator));
	}
	
	protected static void waitForElementApearByLocator(By locator) {
		WebDriverWait wait = new WebDriverWait(driver, 80);
		wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(locator));
	}
	
	protected static void waitForElementApear(WebElement element) {
		WebDriverWait wait = new WebDriverWait(driver, 80);
		wait.until(ExpectedConditions.visibilityOf(element));
	}
	
	protected static void waitForElementApear(WebElement element, Integer timeout) {
		WebDriverWait wait = new WebDriverWait(driver, timeout);
		wait.until(ExpectedConditions.visibilityOf(element));
	}
	
	//I could not make a method to wait for progressBar correctly
//	protected static void waitForProgressBar(By locator){
//		int countTime = 0;
//		while(true){
//			
//			if(countTime >= 600){
//				break;
//			}
//			
//			try {
//				WebElement element = findElement(locator);
//				if(element !=null && element.isDisplayed()){
//						Thread.sleep(500);
//						countTime++;
//				}				
//			}
//			catch (InterruptedException e) {}
//			catch( StaleElementReferenceException ex) {
//				break;
//			}
//			
//		}
//	}
	
	protected static void waitForElementValue(WebElement element, String value){
		WebDriverWait wait = new WebDriverWait(driver, 6000);
		wait.until(ExpectedConditions.textToBePresentInElement(element, value));
	}
	
	public static void closeBrowser(){
		getDriverInstance().quit();
	}
	
	public static void closeAndOpenBrowser(){
//		getDriverInstance().close();
		driver = null;
		initDriverFirefox();
	}
	
	
	protected static WebDriver getDriverInstance() {
		if (driver == null) {
			initDriverFirefox();
		}
		return driver;
	}
	
	/**
	 * Found in http://www.javathinking.com/2013/08/using-selenium-webdriver-to-select.html
	 */
	protected static void selectOptionByText(String elementId, String valueToBeSelected) {
        findElement(By.id(elementId + "_label")).click();
        findElement(By.xpath("//div[@id='" + elementId + "_panel']/div/ul/li[text()='" + valueToBeSelected + "']")).click();
    }
	
	public static boolean isPanelOpen(WebElement element){
		return element.getAttribute("class").contains("active");
	}
	
	
	public String checkCurrentUrl() {
		return driver.getCurrentUrl();
	}
	
	public static String getElementText(WebElement element) {
		if (element == null) {
			return "";
		} else {
			return  element.getText();
		}		
	}
	
	
	private static WebDriver initDriverChrome() {
		System.setProperty("webdriver.chrome.driver",
				"D:/Documentos/MateriaisEstudoGerais/HubShop/Testes/workspace/framework-testes/src/main/resources/chromedriver.exe");
		driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		return driver;
	}
	
	private static WebDriver initDriverFirefox() {
		if (System.getProperty("os.name").contains("Linux")) {
			System.setProperty("webdriver.chrome.driver", "/usr/bin/google-chrome");
		} else {
			System.setProperty("webdriver.chrome.driver",
					"D:/Documentos/MateriaisEstudoGerais/HubShop/Testes/workspace/framework-testes/src/main/resources/chromedriver.exe");
		}
		driver = new FirefoxDriver();
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		return driver;
	}	
}

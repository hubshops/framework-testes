package br.com.hubshop.framework;

import java.util.List;

import org.testng.Assert;

public class BaseTest {
	
	private Object object1;
	
	public BaseTest assertThat(Object object1){
		this.object1 = object1;
		return this;
	}
	
	public void isEqualTo(Object object2){
		Assert.assertEquals(object1,object2);
	}
	
	public void isTrue(){
		if(object1 instanceof Boolean){
			Assert.assertTrue((Boolean) object1);
		}
		else{
			System.out.println(object1.toString() + " should evaluate to a boolean");
		}
	}
	
	public void isFalse(){
		if(object1 instanceof Boolean){
			Assert.assertFalse((Boolean) object1);
		}
		else{
			System.out.println(object1.toString() + " should evaluate to a boolean");
		}
	}
	
	@SuppressWarnings("rawtypes")
	public void hasSize(int size){
		if(object1 instanceof List){
			Assert.assertEquals(size,((List)object1).size());
		}
		else{
			System.out.println(object1.toString() + " should evaluate to a List");
		}
	}
	
}

package br.com.hubshop.framework;



import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class SeleniumTAF {
	private static SeleniumTAF instanciaTAF = null;

	private WebDriver driver;
	
	public WebDriver getDriver() {
		return driver;
	}

	private WebDriver chrome  = null;
	private WebDriver firefox = null;

	private SeleniumTAF() {

	}
	
	public static SeleniumTAF getInstance() {
		if (instanciaTAF == null) {
			instanciaTAF = new SeleniumTAF();
		}
		return instanciaTAF;
	}

	public WebDriver iniciarDriverChrome() {
		System.setProperty("webdriver.chrome.driver",
				"lib\\chromedrive"
				+ ""
				+ "r.exe");
		if (chrome == null) {
			driver = new ChromeDriver();
			chrome = driver;
		} else if (chrome != null) {
			driver = chrome;
		}
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		return driver;
	}
	
	public WebDriver iniciarDriverFirefox() {
		if (firefox == null) {
			driver = new FirefoxDriver();
			firefox = driver;
		} else if (firefox != null) {
			driver = firefox;
		}
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		return driver;
	}
	
	public void navegar(String url) {

		driver.get(url);

		if (driver.getPageSource().contains("Página não encontrada...")) {
			System.err
					.println("Navegou para: "
							+ url
							+ ": Foi direcionada para página não encontrada, confira a url.");
		}

		if (driver.getPageSource().contains("ERROR")) {
			System.err.println("Navegou para: " + url
					+ ": Foi direcionada para página de erro, confira a url.");
		}

	}
	
	public void fechar() {
		driver.quit();
	}
		
	public String pegarUrlCorrente() {

		return driver.getCurrentUrl();

	}
	
}

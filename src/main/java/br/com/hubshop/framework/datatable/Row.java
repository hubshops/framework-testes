package br.com.hubshop.framework.datatable;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class Row{
	
	private WebElement rowElement;

	public Row(WebElement rowElement){
		this.rowElement = rowElement;
	}

	public WebElement getColumnAt(Integer position){
		return rowElement.findElements(By.tagName("td")).get(position); 
	}
	
	public void clickOnCheckBoxAtColumn(Integer position){
		WebElement column = rowElement.findElements(By.tagName("td")).get(position);
		column.findElement(By.tagName("span")).click();
		try {
			Thread.sleep(500);
		} catch (InterruptedException e) {}
	}

	public WebElement getRowElement() {
		return rowElement;
	}

	public void setRowElement(WebElement rowElement) {
		this.rowElement = rowElement;
	}
}

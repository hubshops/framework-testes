package br.com.hubshop.framework.datatable;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;

public class WebTable {
	
	private WebElement webTable;
	
	public WebTable(WebElement webTable){
		setWebTable(webTable);
	}
	
	public WebElement getWebTable() {
		return webTable;
	}
	
	public void setWebTable(WebElement webTable) {
		this.webTable = webTable;
	}
	
	public WebElement getElementAtColumnNumberWithRowText(Integer columnNumber,String text){
		List<WebElement> allRows = getAllRows();
		for(int i = 0; i < allRows.size();++i){
			WebElement row = allRows.get(i);
			if(row != null && row.getText() != null && row.getText().contains(text)){
				return getColumnFromRowAtPosition(row, columnNumber);
			}
		}
		return null;
	}
	
	public Row findRowWithText(String text){
		for(WebElement row : getAllRows()){
			if(row != null && row.getText() != null && row.getText().toUpperCase().contains(text.toUpperCase())){
				return new Row(row);
			}
		}
		return null;
	}
	
	public int getRowCount() {
		List<WebElement> tableRows = webTable.findElements(By.
		tagName("tr"));
		return tableRows.size();
	}
	
	public int getColumnCount() {
		List<WebElement> tableRows = webTable.findElements(By.
		tagName("tr"));
		WebElement headerRow = tableRows.get(0);
		List<WebElement> tableCols = headerRow.findElements(By.
		tagName("td"));
		return tableCols.size();
	}
	
	public WebElement getFirstElement(){
		return getCellAt(2, 1);
	}
	
	private List<WebElement> getAllRows(){
		List<WebElement> tableRows = webTable.findElements(By.
				tagName("tr"));
		//Removing Header
		tableRows.remove(0);
		return tableRows;
	}
	
	public WebElement getColumnFromRowAtPosition(WebElement row,Integer position){
		return row.findElements(By.tagName("td")).get(position); 
	}
	
	public WebElement getCellAt(int rowIdx, int colIdx) throws NoSuchElementException {
		try {
			List<WebElement> tableRows = webTable.findElements(By.
			tagName("tr"));
			WebElement currentRow = tableRows.get(rowIdx-1);
			List<WebElement> tableCols = currentRow.findElements(By.
			tagName("td"));
				WebElement cellCol = tableCols.get(colIdx-1);
			return cellCol;
		} 
		catch (NoSuchElementException e) {
			throw new NoSuchElementException("Failed to get cell editor");
		}
	}
	
}
